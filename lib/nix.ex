defmodule Buildah.Nix.Ghc.Sqlite do

    alias Buildah.{Nix, Cmd}

    def on_container(container, options) do
        Nix.packages_no_cache(container, [
            "ghc",
            "hlint",
            "stack"
        ], options)
        {_, 0} = Cmd.config(
            container,
            env: [
                "DB_TYPE=sqlite"
            ],
            into: IO.stream(:stdio, :line)
        )
    end

    def test(container, image_ID, options) do
        # {_, 0} = Cmd.run(container, ["sh", "-c",
            # "command -v ghc"])
        # {_, 0} = Cmd.run(container, ["sh", "-c",
            # "command -v hlint"])
        # {_, 0} = Cmd.run(container, ["sh", "-c",
            # "command -v stack"])
        # {_, 0} = Podman.Cmd.run(image_ID, [
            # "printenv", "DB_TYPE"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        # {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c",
            # "command -v ghc"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        # {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c",
            # "command -v hlint"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        # {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c",
            # "command -v stack"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
    end

end

